Vintage is a theme based on the basic theme by raincity and the HTML5 boilerplate.I
t was designed to be a starter theme with an attitude for internal use at Appnovation Technologies.
It should be very simple customize, but if you're not experienced I'd recomend editing only styless.css under "costmetic changes"

It uses html5 and CSS3 so don't get your hopes high on validating this OR even getting all the styles working on IE8 and lower.

This was not fully tested on IE6 but it will work fine one 7 and 8. Minus some CSS3 only properties.

You can set it to be used as fixed or fluid width by checking the theme settings page.
To disable the collapsible blocks you can remove the collapsible.js file from th info file. i'm planning to make this a setting on the next update of the theme.

Default width of this theme is 1006px but it should be felxible enought to be shrinked to 960 or even less. Refer to layout.css for those changes.
Dynamic font replacement done with google.com/webfonts - we're planning to make that available on the theme settings page on a future update.


/* the humans responsible & colophon */

/* TEAM */
  Erico Vinicius - Designer / Themer
  Site: ericovinicius.com
  Twitter: @ericonascimento
  Location: Vancouver BC - Canada

/* THANKS */
  - Appnovation Technologies (appnovation.com) for allowing me the time and support to build this.
  - Scott Bell for the patience on answering my questions.

/* SITE */
  Standards: HTML5, CSS3
  Components: Modernizr, jQuery
  

                                    
                               -o/-                       
                               +oo//-                     
                              :ooo+//:                    
                             -ooooo///-                   
                             /oooooo//:                   
                            :ooooooo+//-                  
                           -+oooooooo///-                 
           -://////////////+oooooooooo++////////////::    
            :+ooooooooooooooooooooooooooooooooooooo+:::-  
              -/+ooooooooooooooooooooooooooooooo+/::////:-
                -:+oooooooooooooooooooooooooooo/::///////:-
                  --/+ooooooooooooooooooooo+::://////:-   
                     -:+ooooooooooooooooo+:://////:--     
                       /ooooooooooooooooo+//////:-        
                      -ooooooooooooooooooo////-           
                      /ooooooooo+oooooooooo//:            
                     :ooooooo+/::/+oooooooo+//-           
                    -oooooo/::///////+oooooo///-          
                    /ooo+::://////:---:/+oooo//:          
                   -o+/::///////:-      -:/+o+//-         
                   :-:///////:-            -:/://         
                     -////:-                 --//:        
                       --                       -:        
