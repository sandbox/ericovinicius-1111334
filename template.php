<?php

/**
 *	 This function creates the body classes that are relative to each page
 *	
 *	@param $vars
 *	  A sequential array of variables to pass to the theme template.
 *	@param $hook
 *	  The name of the theme function being called ("page" in this case.)
 */
function vintage_preprocess_page(&$vars, $hook) {

	$vars['footer_copy'] = footer_copyright();

  // Classes for body element. Allows advanced theming based on context
  // (home page, node of certain type, etc.)
  $body_classes = array($vars['body_classes']);
  if (user_access('administer blocks')) {
	  $body_classes[] = 'admin';
	}
  if (!empty($vars['primary_links']) or !empty($vars['secondary_links'])) {
    $body_classes[] = 'with-navigation';
  }
  if (!empty($vars['secondary_links'])) {
    $body_classes[] = 'with-secondary';
  }
  if (module_exists('taxonomy') && $vars['node']->nid) {
		foreach (($vars['node']->taxonomy) as $term) {
			$body_classes[] = 'tax-' . vintage_id_safe(check_plain($term->name));
    }
  }
  if (!$vars['is_front']) {
    // Add unique classes for each page and website section
    $path = drupal_get_path_alias($_GET['q']);
    list($section, ) = explode('/', $path, 2);
    $body_classes[] = vintage_id_safe('page-'. $path);
    $body_classes[] = vintage_id_safe('section-'. $section);

    if (arg(0) == 'node') {
      if (arg(1) == 'add') {
        if ($section == 'node') {
          array_pop($body_classes); // Remove 'section-node'
        }
        $body_classes[] = 'section-node-add'; // Add 'section-node-add'
      }
      elseif (is_numeric(arg(1)) && (arg(2) == 'edit')) {
        if ($section == 'node') {
          array_pop($body_classes); // Remove 'section-node'
        }
        $body_classes[] = 'section-node-edit';
      }
      elseif (is_numeric(arg(1)) && (arg(2) == 'delete')) {
        if ($section == 'node') {
          array_pop($body_classes); // Remove 'section-node'
        }
        $body_classes[] = 'section-node-delete';
      }
    }
  }

  /* Add template suggestions based on content type
   * You can use a different page template depending on the
   * content type or the node ID
   * For example, if you wish to have a different page template
   * for the story content type, just create a page template called
   * page-type-story.tpl.php
   * For a specific node, use the node ID in the name of the page template
   * like this : page-node-22.tpl.php (if the node ID is 22)
   */
  
	if($vars['node']) {
	  $vars['template_files'][] = "page-type-" . $vars['node']->type;
    $vars['template_files'][] = "page-node-" . $vars['node']->nid;
	}
  
	$vars['body_classes'] = implode(' ', $body_classes); // Concatenate with spaces
	
		/***** get the setting of fixed or fluid width ******/
	if (theme_get_setting('vintage_liquidheader')) {
    $vars['header_class'] = 'fluidwidth';
     //drupal_rebuild_theme_registry();
  }
  else {
    $vars['header_class'] = "fixwidth";
     //drupal_rebuild_theme_registry();
  }
	if (theme_get_setting('vintage_liquidbody')) {
    $vars['main_class'] = 'fluidwidth';
     //drupal_rebuild_theme_registry();
  }
  else {
    $vars['main_class'] = "fixwidth";
     //drupal_rebuild_theme_registry();
  }
	if (theme_get_setting('vintage_liquifooter')) {
    $vars['footer_class'] = 'fluidwidth';
     //drupal_rebuild_theme_registry();
  }
  else {
    $vars['footer_class'] = "fixwidth";
     //drupal_rebuild_theme_registry();
  }
}

/**
 *	 This function creates the NODES classes, like 'node-unpublished' for nodes
 *	 that are not published, or 'node-mine' for node posted by the connected user...
 *	
 *	@param $vars
 *	  A sequential array of variables to pass to the theme template.
 *	@param $hook
 *	  The name of the theme function being called ("node" in this case.)
 */
function vintage_preprocess_node(&$vars, $hook) {
  // Special classes for nodes
  $classes = array('node');
  if ($vars['sticky']) {
    $classes[] = 'sticky';
  }
  // support for Skinr Module
  if (module_exists('skinr')) {
    $classes[] = $vars['skinr'];
  }
  if (!$vars['status']) {
    $classes[] = 'node-unpublished';
    $vars['unpublished'] = TRUE;
  }
  else {
    $vars['unpublished'] = FALSE;
  }
  if ($vars['uid'] && $vars['uid'] == $GLOBALS['user']->uid) {
    $classes[] = 'node-mine'; // Node is authored by current user.
  }
  if ($vars['teaser']) {
    $classes[] = 'node-teaser'; // Node is displayed as teaser.
  }
  $classes[] = 'clearfix';
  
  // Class for node type: "node-type-page", "node-type-story", "node-type-my-custom-type", etc.
  $classes[] = vintage_id_safe('node-type-' . $vars['type']);
  $vars['classes'] = implode(' ', $classes); // Concatenate with spaces
	
	
}

/**
 *	This function create the EDIT LINKS for blocks and menus blocks.
 *	When overing a block (except in IE6), some links appear to edit
 *	or configure the block. You can then edit the block, and once you are
 *	done, brought back to the first page.
 *
 * @param $vars
 *   A sequential array of variables to pass to the theme template.
 * @param $hook
 *   The name of the theme function being called ("block" in this case.)
 */ 
function vintage_preprocess_block(&$vars, $hook) {
    $block = $vars['block'];

    // special block classes
    $classes = array('block');
    $classes[] = vintage_id_safe('block-' . $vars['block']->module);
    $classes[] = vintage_id_safe('block-' . $vars['block']->region);
    $classes[] = vintage_id_safe('block-id-' . $vars['block']->bid);
    $classes[] = 'clearfix';
    
    // support for Skinr Module
    if (module_exists('skinr')) {
      $classes[] = $vars['skinr'];
    }
    
    $vars['block_classes'] = implode(' ', $classes); // Concatenate with spaces

    if (theme_get_setting('vintage_block_editing') && user_access('administer blocks')) {
			// Display 'edit block' for custom blocks.
      if ($block->module == 'block') {
        $edit_links[] = l('<span>' . t('edit block') . '</span>', 'admin/build/block/configure/' . $block->module . '/' . $block->delta,
        array(
          'attributes' => array(
          'title' => t('edit the content of this block'),
          'class' => 'block-edit',
        ),
       'query' => drupal_get_destination(),
       'html' => TRUE,
			  )
      );
    }
			// Display 'configure' for other blocks.
			else {
				$edit_links[] = l('<span>' . t('configure') . '</span>', 'admin/build/block/configure/' . $block->module . '/' . $block->delta,
				array(
				 'attributes' => array(
				 'title' => t('configure this block'),
				 'class' => 'block-config',
				 ),
				 'query' => drupal_get_destination(),
				 'html' => TRUE,
				 )
			);
		}
    
		// Display 'edit menu' for Menu blocks.
    if (($block->module == 'menu' || ($block->module == 'user' && $block->delta == 1)) && user_access('administer menu')) {
			$menu_name = ($block->module == 'user') ? 'navigation' : $block->delta;
			$edit_links[] = l('<span>' . t('edit menu') . '</span>', 'admin/build/menu-customize/' . $menu_name,
			array(
				'attributes' => array(
        'title' => t('edit the menu that defines this block'),
        'class' => 'block-edit-menu',
				),
				'query' => drupal_get_destination(),
				'html' => TRUE,
				)
			);
    }
			// Display 'edit menu' for Menu block blocks.
			elseif ($block->module == 'menu_block' && user_access('administer menu')) {
				list($menu_name, ) = split(':', variable_get("menu_block_{$block->delta}_parent", 'navigation:0'));
				$edit_links[] = l('<span>' . t('edit menu') . '</span>', 'admin/build/menu-customize/' . $menu_name,
				array(
					'attributes' => array(
					'title' => t('edit the menu that defines this block'),
					'class' => 'block-edit-menu',
				),
				'query' => drupal_get_destination(),
				'html' => TRUE,
				)
				);
			}
    $vars['edit_links_array'] = $edit_links;
    $vars['edit_links'] = '<div class="edit">' . implode(' ', $edit_links) . '</div>';
		}
  }

/**
 * Override or insert PHPTemplate variables into the block templates.
 *
 *  @param $vars
 *    An array of variables to pass to the theme template.
 *  @param $hook
 *    The name of the template being rendered ("comment" in this case.)
 */
function vintage_preprocess_comment(&$vars, $hook) {
  // Add an "unpublished" flag.
  $vars['unpublished'] = ($vars['comment']->status == COMMENT_NOT_PUBLISHED);

  // If comment subjects are disabled, don't display them.
  if (variable_get('comment_subject_field_' . $vars['node']->type, 1) == 0) {
    $vars['title'] = '';
  }

  // Special classes for comments.
  $classes = array('comment');
  if ($vars['comment']->new) {
    $classes[] = 'comment-new';
  }
  $classes[] = $vars['status'];
  $classes[] = $vars['zebra'];
  if ($vars['id'] == 1) {
    $classes[] = 'first';
  }
  if ($vars['id'] == $vars['node']->comment_count) {
    $classes[] = 'last';
  }
  if ($vars['comment']->uid == 0) {
    // Comment is by an anonymous user.
    $classes[] = 'comment-by-anon';
  }
  else {
    if ($vars['comment']->uid == $vars['node']->uid) {
      // Comment is by the node author.
      $classes[] = 'comment-by-author';
    }
    if ($vars['comment']->uid == $GLOBALS['user']->uid) {
      // Comment was posted by current user.
      $classes[] = 'comment-mine';
    }
  }
  $vars['classes'] = implode(' ', $classes);
}

/**	
 * 	Customize the PRIMARY and SECONDARY LINKS, to allow the admin tabs to work on all browsers
 * 	An implementation of theme_menu_item_link()
 * 	
 * 	@param $link
 * 	  array The menu item to render.
 * 	@return
 * 	  string The rendered menu item.
 */ 	
function vintage_menu_item_link($link) {
  if (empty($link['localized_options'])) {
    $link['localized_options'] = array();
  }

  // If an item is a LOCAL TASK, render it as a tab
  if ($link['type'] & MENU_IS_LOCAL_TASK) {
    $link['title'] = '<span class="tab">' . check_plain($link['title']) . '</span>';
    $link['localized_options']['html'] = TRUE;
  }

  return l($link['title'], $link['href'], $link['localized_options']);
}


/**
 *  Duplicate of theme_menu_local_tasks() but adds clear-block to tabs.
 */
function vintage_menu_local_tasks() {
  $output = '';
  if ($primary = menu_primary_local_tasks()) {
    if(menu_secondary_local_tasks()) {
      $output .= '<ul class="tabs primary with-secondary clearfix">' . $primary . '</ul>';
    }
    else {
      $output .= '<ul class="tabs primary clearfix">' . $primary . '</ul>';
    }
  }
  if ($secondary = menu_secondary_local_tasks()) {
    $output .= '<ul class="tabs secondary clearfix">' . $secondary . '</ul>';
  }
  return $output;
}

/**
 * 	Add custom classes to menu item
 */	
function vintage_menu_item($link, $has_children, $menu = '', $in_active_trail = FALSE, $extra_class = NULL) {
  $class = ($menu ? 'expanded' : ($has_children ? 'collapsed' : 'leaf'));
  if (!empty($extra_class)) {
    $class .= ' '. $extra_class;
  }
  if ($in_active_trail) {
    $class .= ' active-trail';
  }
	
#New line added to get unique classes for each menu item
  $css_class = vintage_id_safe(str_replace(' ', '_', strip_tags($link)));
  return '<li class="'. $class . ' ' . $css_class . '">' . $link . $menu ."</li>\n";
}

/**
 *	Converts a string to a suitable html ID attribute.
 *	
 *	 http://www.w3.org/TR/html4/struct/global.html#h-7.5.2 specifies what makes a
 *	 valid ID attribute in HTML. This function:
 *	
 *	- Ensure an ID starts with an alpha character by optionally adding an 'n'.
 *	- Replaces any character except A-Z, numbers, and underscores with dashes.
 *	- Converts entire string to lowercase.
 *	
 *	@param $string
 *	  The string
 *	@return
 *	  The converted string
 */	
function vintage_id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  // If the first character is not a-z, add 'n' in front.
  if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
    $string = 'id'. $string;
  }
  return $string;
}


/**
 *
 * Override the default breadcrumb markup and add the current page to it
 *
 */
function vintage_breadcrumb($breadcrumb) {
	
	$class = '';
	
  if (!empty($breadcrumb)) {
     $title = drupal_get_title(); 
     if (!empty($title)) {
       $breadcrumb[] = $title;
     } 
     $count = count($breadcrumb);
     $output = "<ul class='contain'>"; 
     $i=0;
     foreach($breadcrumb as $item) { 
       if(++$i == $count) {
         $output .= "<li class='last'><span><a href='#'>" . $item . "</a></span></li>";
       }
       else {
         $output .= "<li ".$class."><span>" . $item . "</span></li> » ";
       }
     }
     $output .= "</ul>";  
     return '<div class="breadcrumb">' . $output . '</div>';
   }
}

/**
 * Add a "Comments" heading above comments except on forum pages.
 */
function vintage_preprocess_comment_wrapper(&$vars) {
  if ($vars['content'] && $vars['node']->type != 'forum') {
    $vars['content'] = '<h2 class="comments pacifico">'. t('Comments') .'</h2>'.  $vars['content'];
  }
}


/**
 *
 * Add a custom class to the links to allow for the color block on the main nav
 *
 */
function vintage_links($links, $attributes = array('class' => 'links')) {
	global $language;
	$output = '';

	if (count($links) > 0) {
		$output = '<ul' . drupal_attributes($attributes) . '>';
		$num_links = count($links);
		$i = 1;
		
		foreach ($links as $key => $link) {
			$class = $key;
		
		// Add first, last and active classes to the list of links to help out themers.
			if ($i == 1) {
				$class .= ' first';
			}
			
			if ($i == $num_links) {
				$class .= ' last';
			}
      
			if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
				&& (empty($link['language']) || $link['language']->language == $language->language)) {
        $class .= ' active';
      }
      
			$output .= '<li' . drupal_attributes(array('class' => $class." item-".$i)) . '>';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      
				else if (!empty($link['title'])) {
					// Some links are actually not links, but we wrap these in <span> for adding title and class attributes
					if (empty($link['html'])) {
						$link['title'] = check_plain($link['title']);
					}
					$span_attributes = '';
					if (isset($link['attributes'])) {
						$span_attributes = drupal_attributes($link['attributes']);
					}
					$output .= '<span' . $span_attributes . '>' . $link['title'] . '</span>';
				}

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
}
 
 
/**
 * Copyright Year
 *
 */
 function footer_copyright(){
    $year = '' ;
    $time = time () ;
    //This line gets the current time off the server
    $year .= date("Y",$time);
    //This line formats it to display just the year
    return t('Copyright ') . $year . ' ' . check_plain(variable_get('site_name', 'drupal')) . '.';
}