$(document).ready(function(){
	
	
	$('div.collapsible').each(function() {
  	var check_fieldset = $(this);
    // Expand if there are errors inside
    if ($('input.error, textarea.error, select.error', check_fieldset).size() > 0) {
      check_fieldset.removeClass('collapsed');
    }
	});
	
	
	$('div.collapsed .inside').hide();
	
	/* $('div.collapsible .handle').click(function(){		
		var fieldset = $(this.parentNode);
		var content = fieldset.children('.inside');
		fieldset.toggleClass('collapsed');
		content.slideToggle('fast');
		Drupal.collapseScrollIntoView2(fieldset);
		return false;
	}); */
	
	$('div.collapsible .handlediv').click(function(){		
		var fieldset = $(this.parentNode);
		var content = fieldset.children('.inside');
		fieldset.toggleClass('collapsed');
		content.slideToggle('fast');
		Drupal.collapseScrollIntoView2(fieldset);
		return false;
	});

});



/**
 * Scroll a given fieldset into view as much as possible.
 */
Drupal.collapseScrollIntoView2 = function (node) {
  var h = self.innerHeight || document.documentElement.clientHeight || $('body')[0].clientHeight || 0;
  var offset = self.pageYOffset || document.documentElement.scrollTop || $('body')[0].scrollTop || 0;
  var posY = $(node).offset().top;
  var fudge = 55;
  if (posY + node.offsetHeight + fudge > h + offset) {
    if (node.offsetHeight > h) {
      window.scrollTo(0, posY);
    } else {
      window.scrollTo(0, posY + node.offsetHeight - h + fudge);
    }
  }
};
